import pymysql
import sqlalchemy
from sqlalchemy import orm
from configparser import ConfigParser

class DBHelper:

    def __init__(self, configuration_path):
        pymysql.install_as_MySQLdb()
        engine = self.__create_engine(configuration_path)
        self.__session = self.__create_session(engine)

    def __create_engine(self, configuration_path):
        database_section = 'database'
        config_parser = ConfigParser()
        config_parser.read(configuration_path)

        host = config_parser.get(database_section, 'host')
        port = config_parser.get(database_section, 'port')
        database = config_parser.get(database_section, 'database')
        username = config_parser.get(database_section, 'username')
        password = config_parser.get(database_section, 'password')

        settings = {'drivername': 'mysql', 'host': host, 'port': port, 'database': database,
                    'username': username, 'password': password, 'query': {'charset': 'utf8mb4'}}

        url = sqlalchemy.engine.url.URL(**settings)
        engine = sqlalchemy.create_engine(url, echo=False)
        return engine

    def __create_session(self, engine):
        session = sqlalchemy.orm.sessionmaker(bind=engine)
        return session()

    def get_session(self):
        return self.__session

    def close(self):
        self.__session.close()
