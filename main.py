from databse import Database
from statistics import count_gloss, count_wikipedia_links, count_emotional_annotations


def run():
    databse = Database('database_configuration.cfg')
    lexical_units = databse.get_lexical_units()

    # gloss_count = count_gloss(lexical_units)
    # print(gloss_count)

    # wikipedia_links_count = count_wikipedia_links(lexical_units)
    # print(wikipedia_links_count)

    emotional_annotation_count = count_emotional_annotations(lexical_units)
    print(emotional_annotation_count)

run()