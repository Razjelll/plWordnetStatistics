class LexicalUnit:

    def __init__(self):
        self.id = None
        self.lemma = None
        self.variant = None
        self.domain = None
        self.pos = None
        self.source = None
        self.comment = None
        self.project = None

        self.lexicon = None
        self.definition = None
        self.emotional_annotations = None # TODO: można stworzyć klasę
        self.link = None


class EmotionalAnnotation:

    def __init__(self, number=None, emotions=None, examples=None):
        self.number = number
        self.emotions = emotions
        self.examples = examples