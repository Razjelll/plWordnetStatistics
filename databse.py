from database_helper import DBHelper
from models import LexicalUnit


class Database:

    def __init__(self, configuration_path):
        self.__db_helper = DBHelper(configuration_path)

    def get_lexical_units(self):

        statement = 'SELECT ID, lemma, domain, pos, source, comment, variant, project FROM lexicalunit'

        result_set = self.__db_helper.get_session().execute(statement)
        units_list = []
        for row in result_set:
            unit = LexicalUnit()
            unit.id = row[0]
            unit.lemma = row[1]
            unit.domain = row[2]
            unit.pos = row[3]
            unit.source = row[4]
            unit.comment = row[5]
            unit.variant = row[6]
            unit.project = row[7]

            units_list.append(unit)

        result_set.close()
        return units_list