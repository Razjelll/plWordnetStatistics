
from databse import Database
import re

from models import EmotionalAnnotation


def count_gloss(units):
    counter = 0
    units_with_definition = []
    units = _filter_polish_units(units)
    print(len(units))
    for unit in units:
        comment = unit.comment
        definition = _get_definition(comment)
        if definition:
            counter += 1
            units_with_definition.append(unit)
    return counter


def _get_definition(comment:str):
    if comment.startswith('#'):
        return _get_from_pattern(comment)
    else:
        return _get_definition_from_plain(comment)

def _get_definition_from_plain(comment:str):
    if 'brak danych' not in comment \
        and not comment.endswith('.') \
        and not comment.startswith('( ang'):
        if '<' in comment:
            index = comment.index('<')
            return comment[:index]
        return comment

def _get_from_pattern(comment):
    definition_pattern = '##D: ?(.*)\.'
    definition_match = re.search(definition_pattern, comment)
    if definition_match:
        definition = definition_match.group(1)
        definition.strip()
        if len(definition) > 0:
            return definition
    return None


def _filter_polish_units(units):
    return [unit for unit in units if _is_pl_unit(unit)]

def _is_pl_unit(unit):
    return unit.pos <= 4

def count_wikipedia_links(units):
    counter = 0
    units_with_links = []
    units = _filter_polish_units(units)

    for unit in units:
        comment = unit.comment
        wikipedia_link = _get_wikipedia_link(comment)
        if wikipedia_link:
            counter += 1
            units_with_links.append(unit)
    return counter

def _get_wikipedia_link(comment):
    link_pattern = '##L: ?(.*)}'
    link_match = re.search(link_pattern, comment)
    if link_match:
        link = link_match.group(1)
        if 'wikipedia' in link:
            return link
    return None

    # TODO: dokończyć
def count_emotional_annotations(units):
    units = _filter_polish_units(units)
    counter = 0
    units_with_annotations = []
    for unit in units:
        comment = unit.comment
        annotations = _get_emotional_annotations(comment)
        if len(annotations) != 0:
            counter += 1
            units_with_annotations.append(unit)
    return counter


def _get_emotional_annotations(comment):
    annotations_results = []
    annotation_pattern = '#(A\d):([^#]*) ?($|#)'
    annotation_iter = re.finditer(annotation_pattern, comment)
    for annotation_match in annotation_iter:
        number = annotation_match.group(1)
        text = annotation_match.group(2)
        emotions = _get_emotions(text)
        examples = _get_annotation_examples(text)

        annotation = EmotionalAnnotation(number, emotions, examples)
        annotations_results.append(annotation)
    return annotations_results



def _get_emotions(annotation_text:str):
    emotions_pattern = '{(.*?)}'
    emotions_match = re.search(emotions_pattern, annotation_text)
    if emotions_match:
        emotions = emotions_match.group(1)
        return emotions.split(';')
    return []

def _get_annotation_examples(annotation_text:str):
    example_pattern = '- s \[(.*?)\]'
    example_match = re.search(example_pattern, annotation_text)
    if example_match:
        example = example_match.group(1)
        example = example.strip()
        if example != '0':
            # TODO: być może trzeba tutaj coś zrobić
            return [example]
        # TODO: sprawdzić, czy może być więcej przykładów
        return [example]
    return []



